<?php

namespace Deputy\SubOrdinate\Domain;

class Role
{
    private $name;
    private $parentRoleId;
    private $id;

    public function __construct(string $name, Role $parentRole = null)
    {
        $this->id = mt_rand();
        $this->name = $name;
        $topLevelRole = 0;
        $this->parentRoleId = $parentRole ? $parentRole->getId(): $topLevelRole;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getParentRoleId(): int
    {
        return $this->parentRoleId;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
