<?php

namespace Deputy\SubOrdinate\Domain;

use ArrayObject;

class UserCollection extends ArrayObject
{
    public function filterByRole(Role $role): UserCollection
    {
        return new UserCollection(
            array_filter($this->getArrayCopy(), function (User $item) use ($role) {
                return $role->getId() === $item->getRole()->getId();
            })
        );
    }

    public function getNamesSorted(): array
    {
        $names = array_map(function (User $item) {
            return $item->getName();
        }, $this->getArrayCopy());

        sort($names);

        return $names;
    }

    public function appendCollection(UserCollection $users): void
    {
        $this->exchangeArray(
            array_merge($this->getArrayCopy(), (array)$users)
        );
    }
}
