<?php

namespace Deputy\SubOrdinate\Domain;

class User
{
    private $name;
    private $role;
    private $id;

    public function __construct(string $name, Role $role)
    {
        $this->id = mt_rand();
        $this->name = $name;
        $this->role = $role;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRole(): Role
    {
        return $this->role;
    }
}
