<?php

namespace Deputy\SubOrdinate\Domain;

use ArrayObject;

class RoleCollection extends ArrayObject
{
    public function filterByParent(Role $role): RoleCollection
    {
        return new RoleCollection(
            array_filter($this->getArrayCopy(), function (Role $item) use ($role) {
                return $role->getId() === $item->getParentRoleId();
            })
        );
    }

    public function getNames(): array
    {
        $names = array_map(function (Role $item) {
            return $item->getName();
        }, $this->getArrayCopy());

        return $names;
    }

    public function filterRecursivelyByParent(Role $role): RoleCollection
    {
        $childrenRoles = $this->filterByParent($role);
        foreach ($childrenRoles->getArrayCopy() as $childRole) {
            $partialRoleCollection = $this->filterRecursivelyByParent($childRole);
            $childrenRoles->appendCollection($partialRoleCollection);
        }

        return $childrenRoles;
    }

    public function appendCollection(RoleCollection $roleCollection): void
    {
        $this->exchangeArray(
            array_merge($this->getArrayCopy(), (array)$roleCollection)
        );
    }
}
