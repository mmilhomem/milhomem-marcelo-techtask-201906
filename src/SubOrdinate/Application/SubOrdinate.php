<?php

namespace Deputy\SubOrdinate\Application;

use Deputy\SubOrdinate\Domain\RoleCollection;
use Deputy\SubOrdinate\Domain\User;
use Deputy\SubOrdinate\Domain\UserCollection;

class SubOrdinate
{
    /** @var RoleCollection */
    private $roles;
    /** @var UserCollection */
    private $users;

    public function __construct(RoleCollection $roles, UserCollection $users)
    {
        $this->roles = $roles;
        $this->users = $users;
    }

    public function usersOf(User $user): UserCollection
    {
        $subOrdinatedRoles = $this->roles->filterRecursivelyByParent($user->getRole());

        $usersOfRole = new UserCollection();
        foreach ($subOrdinatedRoles as $subOrdinateRole) {
            $usersOfRole->appendCollection($this->users->filterByRole($subOrdinateRole));
        }

        return $usersOfRole;
    }
}
