#!make

test: vendor code-style
	$(call docker_run,php,./vendor/bin/phpunit)

help:
	@echo "available actions:"
	@echo "    test"
	@echo "        Test the project"
	@echo "    code-style"
	@echo "        Test if the project comply with our code style"

code-style:
	$(call docker_run,php,./vendor/bin/phpcs)

vendor: composer.json composer.lock vendor/autoload.php
	@echo "Missing dependency $@"
	$(call docker_run,composer,install)

vendor/autoload.php:
	@echo "Missing essential dependency $@, cleaning incomplete vendor folder"
	-rm -rf vendor

DOCKER_COMPOSE_BIN = $(shell command -v docker-compose 2> /dev/null)
docker_compose = ${DOCKER_COMPOSE_BIN} $(1)
docker_run = $(call docker_compose,run --rm $(1) $(2))
