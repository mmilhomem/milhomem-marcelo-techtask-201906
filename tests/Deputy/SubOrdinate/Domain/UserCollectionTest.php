<?php

namespace Test\Deputy\SubOrdinate\Domain;

use Deputy\SubOrdinate\Domain\Role;
use Deputy\SubOrdinate\Domain\User;
use Deputy\SubOrdinate\Domain\UserCollection;
use PHPUnit\Framework\TestCase;

class UserCollectionTest extends TestCase
{
    public function providesUsers()
    {
        $singleUserRole = new Role('Kangaroo');
        $koalaUser = new User('Koala', $singleUserRole);

        $multipleUsersRole = new Role('Marsupials');
        $platypusUser = new User('Platypus', $multipleUsersRole);
        $wombatUser = new User('Wombat', $multipleUsersRole);

        $multipleLevelRole = new Role('Emu');
        $possumUser = new User('Possum', $multipleLevelRole);
        $roleWithParent = new Role('Wallaby', $multipleLevelRole);
        $wallabyUser = new User('Wallaby', $roleWithParent);

        $roleWithoutUsers = new Role('Parrot');

        $allUsers = new UserCollection([
            $koalaUser,
            $platypusUser,
            $wombatUser,
            $possumUser,
            $wallabyUser
        ]);

        return [
            'When a role has one user' => [
                'Given one user' => new UserCollection([$koalaUser]),
                'Searching for a' => $singleUserRole,
                'It finds the child' => [$koalaUser->getName()]
            ],
            'When a role has more than one user' => [
                'Given a collection of' => $allUsers,
                'Searching for a' => $multipleUsersRole,
                'It finds these children' => [$platypusUser->getName(), $wombatUser->getName()]
            ],
            'When a role has more than one level of roles' => [
                'Given a collection of' => $allUsers,
                'Searching for a' => $multipleLevelRole,
                'It does not transverse and finds only' => [$possumUser->getName()]
            ],
            'When a role cannot be found in an empty collection' => [
                'Given a collection of' => new UserCollection(),
                'Searching for a' => $singleUserRole,
                'It returns an empty result' => []
            ],
            'When the role is present but has no users' => [
                'Given a collection of' => $allUsers,
                'Searching for a' => $roleWithoutUsers,
                'It returns an empty result' => []
            ]
        ];
    }

    /**
     * @dataProvider providesUsers
     */
    public function testWhenUserHasOneParentItFindsTheChild(UserCollection $roles, $role, $expectedResult)
    {
        $resultUsers = $roles->filterByRole($role);

        $this->assertCount(count($expectedResult), $resultUsers, 'The amount of users to find is not correct');
        $this->assertEquals(
            $expectedResult,
            array_values($resultUsers->getNamesSorted()),
            sprintf('The user name was not found as a child in [%s]', join(',', $resultUsers->getNamesSorted()))
        );
    }
}
