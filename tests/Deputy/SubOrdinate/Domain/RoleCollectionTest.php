<?php

namespace Test\Deputy\SubOrdinate\Domain;

use Deputy\SubOrdinate\Domain\Role;
use Deputy\SubOrdinate\Domain\RoleCollection;
use PHPUnit\Framework\TestCase;

class RoleCollectionTest extends TestCase
{
    public function providesRolesWithChildren()
    {
        $singleChildRole = new Role('Kangaroo');
        $koalaRole = new Role('Koala', $singleChildRole);

        $multipleChildrenRole = new Role('Marsupials');
        $platypusRole = new Role('Platypus', $multipleChildrenRole);
        $wombatRole = new Role('Wombat', $multipleChildrenRole);

        $multipleLevelChildrenRole = new Role('Emu');
        $wallabyRole = new Role('Wallaby', $multipleLevelChildrenRole);
        $possumRole = new Role('Possum', $wallabyRole);

        $allRoles = new RoleCollection([
            $koalaRole,
            $singleChildRole,
            $multipleChildrenRole,
            $platypusRole,
            $wombatRole,
            $multipleLevelChildrenRole,
            $wallabyRole,
            $possumRole
        ]);

        return [
            'When a role has only one child' => [
                'Given roles with two elements' => new RoleCollection([$koalaRole, $singleChildRole]),
                'Searching for a' => $singleChildRole,
                'It finds the child' => [$koalaRole->getName()]
            ],
            'When a role has more than one child' => [
                'Given a collection of' => $allRoles,
                'Searching for a' => $multipleChildrenRole,
                'It finds these children' => [$platypusRole->getName(), $wombatRole->getName()]
            ],
            'When a role has more than one level of roles' => [
                'Given a collection of' => $allRoles,
                'Searching for a' => $multipleLevelChildrenRole,
                'It does not transverse and finds' => [$wallabyRole->getName()]
            ],
            'When a role cannot be found in the collection' => [
                'Given a collection of' => new RoleCollection(),
                'Searching for a' => $singleChildRole,
                'It does returns an empty result' => []
            ],
            'When the role is present but has no child' => [
                'Given a collection of' => $allRoles,
                'Searching for a' => $possumRole,
                'It returns an empty result' => []
            ]
        ];
    }

    /**
     * @dataProvider providesRolesWithChildren
     */
    public function testGetRolesFromParent(RoleCollection $roles, $parentRole, $expectedResult)
    {
        $resultRoles = $roles->filterByParent($parentRole);

        $this->assertCount(count($expectedResult), $resultRoles, 'The amount of roles to find is not correct');
        $this->assertEquals(
            $expectedResult,
            array_values($resultRoles->getNames()),
            sprintf('The role name was not found as a child in [%s]', join(',', $resultRoles->getNames()))
        );
    }
}
