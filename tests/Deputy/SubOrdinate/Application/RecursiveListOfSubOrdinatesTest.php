<?php

namespace Test\Deputy\SubOrdinate\Application;

use Deputy\SubOrdinate\Application\SubOrdinate;
use Deputy\SubOrdinate\Domain\Role;
use Deputy\SubOrdinate\Domain\RoleCollection;
use Deputy\SubOrdinate\Domain\User;
use Deputy\SubOrdinate\Domain\UserCollection;
use PHPUnit\Framework\TestCase;

class RecursiveListOfSubOrdinatesTest extends TestCase
{
    public function testRoleWithNoSubOrdinateWillReturnEmptyResult()
    {
        $role = new Role('Kangaroo');
        $roles = new RoleCollection();
        $roles->append($role);

        $users = new UserCollection();
        $user = new User('Darwin', $role);
        $users->append($user);

        $subOrdinate = new SubOrdinate($roles, $users);

        $this->assertCount(0, $subOrdinate->usersOf($user));
    }

    public function testRoleWithOneSubOrdinatedUserWillReturnOneUser()
    {
        $parentRole = new Role('Kangaroo');
        $role = new Role('Koala', $parentRole);
        $roles = new RoleCollection();
        $roles->append($role);

        $parentUser = new User('Melbourne', $parentRole);
        $user = new User('Cairns', $role);
        $users = new UserCollection([$parentUser, $user]);

        $subOrdinate = new SubOrdinate($roles, $users);
        $resultUsers = $subOrdinate->usersOf($parentUser);

        $this->assertCount(1, $resultUsers, 'The amount of users to find is not correct');
        $this->assertEquals(
            [$user->getName()],
            $resultUsers->getNamesSorted(),
            sprintf('The user name was not found as a child in [%s]', join(',', $resultUsers->getNamesSorted()))
        );
    }

    public function testSingleLevelRoleWithMultipleSubOrdinatesWillFindAllUsersOnThatChildRole()
    {
        $parentRole = new Role('Kangaroo');
        $role = new Role('Koala', $parentRole);
        $roles = new RoleCollection();
        $roles->append($role);

        $parentUser = new User('Melbourne', $parentRole);
        $cairnsUser = new User('Cairns', $role);
        $newcastleUser = new User('Newcastle', $role);
        $users = new UserCollection([$cairnsUser, $newcastleUser, $parentUser]);

        $subOrdinate = new SubOrdinate($roles, $users);
        $resultUsers = $subOrdinate->usersOf($parentUser);

        $this->assertCount(2, $resultUsers, 'The amount of users to find is not correct');
        $this->assertEquals(
            [$cairnsUser->getName(), $newcastleUser->getName()],
            $resultUsers->getNamesSorted(),
            sprintf('The user name was not found as a child in [%s]', join(',', $resultUsers->getNamesSorted()))
        );
    }

    public function testMultiLevelRoleWithSingleUsersWillFindOneUserOfEachChildRole()
    {
        $parentRole = new Role('Kangaroo');
        $koalaRole = new Role('Koala', $parentRole);
        $possumRole = new Role('Possum', $parentRole);
        $wallabyRole = new Role('Wallaby', $parentRole);
        $roles = new RoleCollection([$parentRole, $koalaRole, $possumRole, $wallabyRole]);

        $parentUser = new User('Melbourne', $parentRole);
        $userWithKoalaRole = new User('Cairns', $koalaRole);
        $userWithPossumRole = new User('Hobart', $possumRole);
        $userWithWallabyRole = new User('Wollongong', $wallabyRole);
        $users = new UserCollection([$userWithKoalaRole, $userWithPossumRole, $userWithWallabyRole]);

        $subOrdinate = new SubOrdinate($roles, $users);
        $resultUsers = $subOrdinate->usersOf($parentUser);

        $this->assertCount(3, $resultUsers, 'The amount of users to find is not correct');
        $this->assertEquals(
            $users,
            $resultUsers,
            sprintf('The user name was not found as a child in [%s]', join(',', $resultUsers->getNamesSorted()))
        );
    }

    public function testSingleBranchRolesWithSingleUsersWillFindOneUserOfEachChildRole()
    {
        $parentRole = new Role('Kangaroo');
        $koalaRole = new Role('Koala', $parentRole);
        $possumRole = new Role('Possum', $koalaRole);
        $wallabyRole = new Role('Wallaby', $possumRole);
        $roles = new RoleCollection([$parentRole, $koalaRole, $possumRole, $wallabyRole]);

        $parentUser = new User('Melbourne', $parentRole);
        $userWithKoalaRole = new User('Cairns', $koalaRole);
        $userWithPossumRole = new User('Hobart', $possumRole);
        $userWithWallabyRole = new User('Wollongong', $wallabyRole);
        $users = new UserCollection([$userWithKoalaRole, $userWithPossumRole, $userWithWallabyRole]);

        $subOrdinate = new SubOrdinate($roles, $users);
        $resultUsers = $subOrdinate->usersOf($parentUser);

        $this->assertCount(3, $resultUsers, 'The amount of users to find is not correct');
        $this->assertEquals(
            $users->getNamesSorted(),
            $resultUsers->getNamesSorted(),
            sprintf('The user name was not found as a child in [%s]', join(',', $resultUsers->getNamesSorted()))
        );
    }
}
