<?php

namespace Test\Deputy\SubOrdinate\Application;

use Deputy\SubOrdinate\Application\SubOrdinate;
use Deputy\SubOrdinate\Domain\Role;
use Deputy\SubOrdinate\Domain\RoleCollection;
use Deputy\SubOrdinate\Domain\User;
use Deputy\SubOrdinate\Domain\UserCollection;
use PHPUnit\Framework\TestCase;

class RecursiveListOfSubOrdinatesProvidedTest extends TestCase
{
    /** @var SubOrdinate $subOrdinate */
    private $subOrdinate;
    private $users;

    protected function setUp(): void
    {
        $roles = new RoleCollection([
            $role1 = new Role("System Administrator"),
            $role2 = new Role("Location Manager", $role1),
            $role3 = new Role("Supervisor", $role2),
            $role4 = new Role("Employee", $role3),
            $role5 = new Role("Trainer", $role3),
        ]);

        $this->users = new UserCollection([
            new User("Adam Admin", $role1),
            new User("Emily Employee", $role4),
            new User("Sam Supervisor", $role3),
            new User("Mary Manager", $role2),
            new User("Steve Trainer", $role5)
        ]);

        $this->subOrdinate = new SubOrdinate($roles, $this->users);
    }

    public function testGivenUser3ItWillResultInObjUser2AndObjUser5()
    {
        $resultUsers = $this->subOrdinate->usersOf($this->getUser(3));

        $this->assertCount(2, $resultUsers, 'The amount of users to find is not correct');
        $this->assertEquals(
            [$this->getUser(2)->getName(), $this->getUser(5)->getName()],
            $resultUsers->getNamesSorted(),
            sprintf('The user name was not found as a child in [%s]', join(',', $resultUsers->getNamesSorted()))
        );
    }

    private function getUser(int $id): User
    {
        $index = $id - 1;
        return $this->users[$index];
    }

    public function testGivenUser1ItWillResultInObjUsersFrom2To5()
    {
        $resultUsers = $this->subOrdinate->usersOf($this->getUser(1));

        $this->assertCount(4, $resultUsers, 'The amount of users to find is not correct');
        $this->assertEquals(
            [
                $this->getUser(2)->getName(),
                $this->getUser(4)->getName(),
                $this->getUser(3)->getName(),
                $this->getUser(5)->getName(),
            ],
            $resultUsers->getNamesSorted(),
            sprintf('The user name was not found as a child in [%s]', join(',', $resultUsers->getNamesSorted()))
        );
    }
}
